def da_boas_vindas
    puts "================================="
    puts "Bem vindo ao jogo da adivinhação"
    puts "================================="
    puts "Escreva o seu nome: "
    nome = gets.strip
    puts "Estamos começando o jogo pra você, #{nome}" 
    #nome
end

def pede_dificuldade 
    puts "============================================"
    puts"Digite o nível de dificuldade que você deseja: \n1- Muito Fácil\n2- Fácil"
    puts"3- Médio\n4- Difícil\n5- Muito Difícil"
    puts "============================================"
    dificuldade = gets.to_i
end


def sorteia_numero_secreto(dificuldade) 
    
    case dificuldade
    when 1 
        maximo = 30
    when 2
        maximo = 60
    when 3
        maximo = 100
    when 4 
        maximo = 150
    else
        maximo = 200
    end
    puts "\noooooooooooooooooooooooooooooooooooooooooooooooo"
    puts "\nGerando um número secreto entre 0 e #{maximo}..."
    sorteado = rand(maximo) + 1
    puts "Pronto!!!\nQue tal você advinhar agora o número secreto?"
    sorteado
end

def pede_um_numero chutes,tentativa, lim_tentativas
    puts "\nTentativa #{tentativa} de #{lim_tentativas}."
    puts "Chutes até agora: #{chutes}"
    puts "Entre com o número:"
    chute = gets.strip
    puts "\nVocê chutou #{chute}.\n e você "
    chute.to_i
end

def verifica_se_acertou chute, num_secreto
    acertou = num_secreto == chute.to_i
    if acertou
        puts "((((((((((((((======IUPIIIIIIII!!!!!======))))))))))))))"
        puts "(((((((((((((((((===VOCÊ ACERTOU!!!!===)))))))))))))))))"
        return true
    end
        
        maior = num_secreto < chute.to_i
            if maior
                puts "============\nVocê errou!\n============"
                puts "\nDica:\nO número secreto é maior!"               
            else
                puts "============\nVocê errou!\n============"
                puts "\nDica:\nO número secreto é menor!"               
            end   
end
def joga(nome, dificuldade)
    num_secreto = sorteia_numero_secreto dificuldade
    pontos_ate_agora = 1000
    
    lim_tentativas = 5
    chutes = []
    
    
    for tentativa in 1..lim_tentativas
        chute = pede_um_numero chutes, tentativa, lim_tentativas
        chutes << chute
       
        pontos_a_perder = (chute - num_secreto).abs / 2.0
        pontos_ate_agora -= pontos_a_perder
        
        if verifica_se_acertou num_secreto, chute
            break
        end
    end
    puts "\nO número secreto era #{num_secreto}."
    puts "Você ganhou #{pontos_ate_agora} pontos."   
end

def nao_quer_jogar?
    puts "\n----------------------------"
    puts "Deseja jogar novamente? (S/N)"
    puts "******************************"
    nao_quero_jogar = gets.strip
    nao_quero_jogar = nao_quero_jogar.upcase == "N"
end

nome = da_boas_vindas
dificuldade = pede_dificuldade

loop do 
    joga nome, dificuldade
    if nao_quer_jogar?
        break
    end
end
    puts "==========================="
    puts "======Fim de Jogo!!!======="
    puts "=======ATÉ BREVE!!!========"
    puts "==========================="